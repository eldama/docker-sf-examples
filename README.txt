A set of ready-to-go docker configurations for your symfony apps. Only requires minimal modifications and you are off !
There is multiple branches according to your project version.

This configuration contains the following images:
	- postgresql
	- rabbitmq
	- php-fpm
	- nginx